package com.merck;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import com.merck.ExtractMaster;
import com.util.DbUtil;

@SuppressWarnings("unchecked")
public class Pdf_to_image {
    public static void main(String[] args) {
        try {
        	List<ExtractMaster> submaster = new ArrayList<ExtractMaster>();
        	String sourceDir = "D:/My_Work/JIRA_Tasks/CRYS-383_TokyoCentury/Files_2/"; // Pdf files are read from this folder
        	String destinationDir = "D:/My_Work/JIRA_Tasks/CRYS-383_TokyoCentury/Files_2/output/"; // converted images from pdf document are saved here
        	
        	File filedir = new File(sourceDir);
        	File destinationFile = new File(destinationDir);
        	
        	if (!destinationFile.exists()) {
        		destinationFile.mkdir();
        		System.out.println("Folder Created -> "+ destinationFile.getAbsolutePath());
        		}
        	
        	for(File sourceFile : filedir.listFiles()){
        		String pdffileName = sourceFile.getName();
        		System.out.println(pdffileName);
        		String fileExt = FilenameUtils.getExtension(pdffileName);
        		
        		if (sourceFile.exists() && fileExt.equalsIgnoreCase("pdf")) {
        			System.out.println("Images copied to Folder: "+ destinationFile.getName());
        			System.out.println(sourceFile.getAbsolutePath());
        			PDDocument document = PDDocument.load(sourceFile);
        			List<PDPage> list = document.getDocumentCatalog().getAllPages();
        			System.out.println("Total files to be converted -> "+ list.size());

            String fileName = sourceFile.getName().replace(".pdf", "");             
            int pageNumber = 1;
            for (PDPage page : list) {
            	ExtractMaster extractMaster = new ExtractMaster();
                BufferedImage image = page.convertToImage();
                String outputfile = fileName + " - " + String.format("%04d", pageNumber);
                System.out.println(outputfile);
                String outputfiletxt = outputfile + ".txt";
                System.out.println(outputfiletxt);
                File outputfilepng = new File(destinationDir + outputfile +".png");
                
                extractMaster.setFilename(pdffileName);
            	extractMaster.setPagenumber(pageNumber);
                extractMaster.setId(outputfiletxt);
                extractMaster.setImagepath(outputfilepng.getName());
                submaster.add(extractMaster);
                System.out.println("Image Created -> "+ outputfilepng.getName());
                ImageIO.write(image, "png", outputfilepng);
                pageNumber++;
            }
            document.close();
            System.out.println("Converted Images are saved at -> "+ destinationFile.getAbsolutePath());
        } else {
            System.err.println(sourceFile.getName() +" not a pdf file or may be a directory");
        }
        }
        Pdf_to_image.extractandInsertToPostgres(submaster);

    } catch (Exception e) {
        e.printStackTrace();
    }
}
    
    
    public static void extractandInsertToPostgres(List<ExtractMaster> extractmaster) throws SQLException, IOException {
		Connection con = null;
		PreparedStatement ps = null;
		DbUtil dbUtil = new DbUtil();
		con = dbUtil.getPostGresConnection();
		
		String insertQuery = "insert into tokyo(id,filepath,pngpath,page_number) values(?,?,?,?)";
		try {
			ps = con.prepareStatement(insertQuery);
			int counter = 1;
			for(int i=0; i<extractmaster.size(); i++){
				ps.setString(1, extractmaster.get(i).getId());
				ps.setString(2, extractmaster.get(i).getFilename());
				ps.setString(3, extractmaster.get(i).getImagepath());
				ps.setInt(4, extractmaster.get(i).getPagenumber());
				ps.addBatch();
				
				if(i %100 == 0){
					ps.executeBatch();
					System.out.println("Batch " + (counter++) + " executed");
				}
			}
			ps.executeBatch();
			ps.clearBatch();
		} catch (BatchUpdateException ex){
			throw ex.getNextException();
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			}
			if (con != null) {
				con.close();
			}
		}
	}
}