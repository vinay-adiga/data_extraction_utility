package com.merck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FilenameUtils;

import com.util.DbUtil;

public class TextExtractFromText {

	public static void main(String[] args) throws Exception {
		
		String file = "D:/My_Work/JIRA_Tasks/CRYS-383_TokyoCentury/Output_txt_files/Output/";
		TextExtractFromText tet = new TextExtractFromText();
		System.out.println("Choose your option : ");
		System.out.println("1. using buffered reader for line by line ");
		System.out.println("2. read text as string");
		Scanner sc = new Scanner(System.in);
		int selection = sc.nextInt();
		switch(selection){
		case 1 : tet.readTxtFile(file);
				break;
				
		case 2 : TextExtractFromText.readFileAsString(file);
				//TextExtractFromText.extractandInsertToPostgres(mainmaster);
				break;
		
		default : System.out.println("wrong option selected... ");
		
		}
	}
	
	public static void extractandInsertToPostgres(List<ExtractMaster> extractmaster) throws SQLException, IOException{
		Connection con = null;
		PreparedStatement ps = null;
		DbUtil dbUtil = new DbUtil();
		con = dbUtil.getPostGresConnection();
		
		String insertQuery = "update tokyo set rawtext = ? where id = ?";
		try {
			ps = con.prepareStatement(insertQuery);
			int counter = 1;
			for(int i=0; i<extractmaster.size(); i++){
				ps.setString(1, extractmaster.get(i).getRawtext());
				ps.setString(2, extractmaster.get(i).getId());
				ps.addBatch();
				
				if(i %100 == 0){
					ps.executeBatch();
					System.out.println("Batch " + (counter++) + " executed");
				}
			}
			ps.executeBatch();
			ps.clearBatch();
		} catch (BatchUpdateException ex){
			throw ex.getNextException();
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			}
			if (con != null) {
				con.close();
			}
		}
	
	}
	
	public void readTxtFile(final String inputDir) throws IOException{
		File txtfile = new File(inputDir);
		for(File sourceFile : txtfile.listFiles()){
			String fileExt = FilenameUtils.getExtension(sourceFile.getName());
			if (sourceFile.isDirectory()){
				readTxtFile(sourceFile.getAbsolutePath());
			}
			else if(fileExt.equalsIgnoreCase("txt")){
			System.out.println("file name is : " + sourceFile.getName());
			BufferedReader br = new BufferedReader(new FileReader(sourceFile.getAbsolutePath()));
			 
			  String st;
			  while ((st = br.readLine()) != null)
			    System.out.println(st);
			  
			  br.close();
			}
		}
		
	}
	
	public static void readFileAsString(String fileName)throws Exception
	  {
		List<ExtractMaster> submaster = new ArrayList<ExtractMaster>();
	    String data = "";
	    File txtfile = new File(fileName);
		for(File sourceFile : txtfile.listFiles()){
			String fileExt = FilenameUtils.getExtension(sourceFile.getName());
			if (sourceFile.isDirectory()){
				readFileAsString(sourceFile.getAbsolutePath());
			}
			else if(fileExt.equalsIgnoreCase("txt")){
				ExtractMaster extractMaster = new ExtractMaster();
				//System.out.println("file name is : ---------- " + sourceFile.getName());
				data = new String(Files.readAllBytes(Paths.get(sourceFile.getAbsolutePath())));
				//System.out.println(data);
				extractMaster.setId(sourceFile.getName());
				extractMaster.setRawtext(data);
				submaster.add(extractMaster);
			}
		}
		TextExtractFromText.extractandInsertToPostgres(submaster);
	  }

}
