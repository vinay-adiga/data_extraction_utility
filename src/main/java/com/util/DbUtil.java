package com.util;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DbUtil {
	DbUtil dbUtil = null;
	public static boolean isDbUtilInitialized = false;
	private Properties prop = null;

	public Properties getProp() {
		return prop;
	}

	public DbUtil() throws IOException {
		FileReader reader = new FileReader("config.properties");
		prop = new Properties();
		prop.load(reader);
		if (!isDbUtilInitialized)
			initialize();
	}

	private static void initialize() {
		isDbUtilInitialized = true;
	}

	public Connection getPostGresConnection() {
		Connection connection = null;
		final String DBURL = prop.getProperty("DBURL");
		final String DBUSER = prop.getProperty("DBUSER");
		final String DBPASSWORD = prop.getProperty("DBPASSWORD");

		try {

			connection = DriverManager.getConnection(DBURL, DBUSER, DBPASSWORD);
			System.out.println("Using Postgress connection...");

		} catch (Exception ee) {
			System.out.println("Exception" + ee.getMessage());
			System.out.println("Exception in detail" + ee);
			System.out.println(ee.getMessage());
			ee.printStackTrace();
		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}

		return connection;
	}
}
